#!/usr/bin/env python

import readline
import sys
import string
import random
from modules.basic import *
from modules.grammar import *
from modules.colors import *
import modules.prec as prec

### GLOBALS

# last opened file name
LASTFILE = '' 

### READLINE STUFF
def setInputText(text):
	# hook for setting input text
	def hook():
		readline.insert_text(text)
		readline.redisplay()
		readline.set_pre_input_hook( None )

	readline.set_pre_input_hook( hook )

def readInput(prompt = '', default = ''):
	if default: setInputText(default)
	return raw_input(prompt)
	

def printRules(rules):
	for i, r in enumerate(rules):
		print("%2d: %s" % (i, r))

def readRulesFromFile(filename):
	if not filename: return []
	rules = []
	with open(filename, 'r') as f:
		for line in f:
			# skip empty lines or comments
			if not line.strip() or line.strip()[0] == '#': continue
			rules.append(Rule.fromStr(line.strip()))

	return rules

def writeRulesToFile(rules, filename):
	if not filename: return
	with open(filename, 'w') as f:
		for r in rules:
			f.write('%s\n' % r.getEditStr())




#######
# MENU
def addRule(rules):
	print(BLUE('Add rule'))
	print('Rule form: A -> bCd')
	print('(upper-case letters = nonterminals, lower-case letters = terminals, "eps" = epsilon)')
	print('(leave blank to stop)')

	while True:
		try:
			input = readInput('New rule: ')

			if input == '':
				return

			rule = Rule.fromStr(input)
			rules.append(rule)
			print(OK("Rule %s added successfully (leave blank to stop)") % (rule))

		except:
			print(FAIL('Error: ') + str(sys.exc_info()[1]) )


def delRule(rules):
	print(BLUE('Delete Rule'))

	input = readInput('Rule number (leave blank to cancel):')
	if input == '': return

	r = rules[int(input)]
	del rules[int(input)]

	print(OK('Rule %s: [%s] deleted') % (input, r))
	return


def editRule(rules, i):
	print(BLUE('Edit Rule'))
	oldr = rules[i]
	input = readInput('Edit Rule %d: [%s]: ' % ( i, oldr ), oldr.getEditStr() )
	rules[i] = Rule.fromStr(input)

	print(OK('Edit [%s] => [%s] successfull') % (oldr, rules[i]))
	
	

def openFile(defaultfile = ''):
	print(BLUE('Open File'))
	print('Read rules from file (leave blank to cancel)')
	filename = readInput('filename: ', defaultfile)
	if not filename: return []

	rules = readRulesFromFile(filename)
	LASTFILE = filename

	print(OK('Read successfull'))
	return rules

def writeFile(rules, defaultfile = ''):
	print(BLUE('Write File'))

	print('Write rules to file (leave blank to cancel)')
	filename = readInput('filename: ', defaultfile)
	if not filename: return
	
	writeRulesToFile(rules, filename)
	LASTFILE = filename
	print(OK('Write successfull'))


def doGP(G):
	print(BLUE('Simulate General Parsing'))

	if not G.isCNF(): print(WARN('Warning: grammar is not in CNF!'))
	string = readInput('String to parse (up to 10 characters, leave blank to cancel): ')
	if string == '': return
	G.printGPParse(string)
	readInput('(press Enter to continue)');
	return

def precTable():
	print(BLUE('Calculate precedence table'))
	print('Eneter operators, priorities and associativity (leave blank to cancel)')
	print('Example:')
	print('     operators: "+-*/^"\t(one symbol = one operator)')
	print('      priority: "11223"\t(higher number = higher priority)')
	print(' associativity: "llllr"\t("l" for left, "r" for right)')
	print

	op = readInput('    Operators: ')
	if op == '': return
	p  = readInput('   Priorities: ')
	if p == '': return
	a  = readInput('Associativity: ')
	if a == '': return

	print
	print('Precedence Table:')
	prec.printTable(op, p, a)
	readInput('(press Enter to continue)')




def randomize(G):
	print(BLUE('Randomize symbols'))
	print('Randomize nonterminals, terminals and rule order (starting rule will always be first)')
#	print('Leave any input blank to cancel')
	
	nont = readInput('Nonterminals to randomize: ', ''.join(sorted(G.N)))
#	if nont == '': return G.P
	
	newnont = readInput('New nonterminal symbols: ', ''.join(random.sample(string.ascii_uppercase, len(nont))))
#	if newnont == '': return G.P
	
	term = readInput('Terminals to randomize: ', ''.join(sorted(G.T)))
#	if term == '': return G.P
	
	newterm = readInput('New terminal symbols: ', ''.join(random.sample(string.ascii_lowercase, len(term))))
#	if newterm == '': return G.P
	
	# randomize new nonterminals and terminal
	newnont = ''.join(random.sample(newnont, len(newnont)))
	newterm = ''.join(random.sample(newterm, len(newterm)))
	
	# create translation table
	tab = string.maketrans(nont + term, newnont + newterm)
	
	# translate and randomize all rules but starting one
	rules = [G.P[0].translate(tab)] + random.sample( [r.translate(tab) for r in G.P[1:]], len(G.P)-1 )
	
	print(OK('Randomization successfull: [%s] => [%s], [%s] => [%s]') % (nont, newnont, term, newterm))
	return rules


def printMenu():
	help = [
		[
		"Rules:",
		"[a] Add Rules", 
		"[d] Delete Rule", 
		"[c] Clear All Rules", 
		"[r] Randomize symbols",
		"[:num:] Edit Rule :num:", 
		],
		[
		"System:",
		"[o] Open rules file", 
		"[w] Write rules file", 
		"[x] Exit",
		],
		[
		"Grammar:",
		"[p]  Print grammar",
		"[ll] Compute LL tables", 
		"[lr] Compute LR tables", 
		"[gp] Simulate GP", 
		],
		[
		"Other:",
		"[pt] Precedence table",
		],
	] 

	# get maximum item width and create format string accordingly
	width = max([len(row) for group in help for row in group]) + 5
	form = ('{:<%d}' % width) * len(help)

	# distribute gruops to rows
	rows = map(None, *help)

	# print header in blue
	print(BLUE(form.format(*rows[0])))

	# print rest in normal
	for row in rows[1:]:
		# substitute None for ''
		rowdata = [x if x else '' for x in row]
		print(form.format(*rowdata))


def menu():
	
	rules = readRulesFromFile(LASTFILE)

	# commands that dont need rules
	norules = {'x', 'a', 'c', 'o', 'w', 'pt'}
	
# main loop
	while True:
		try:
			printRules(rules)
			printMenu()
			input = readInput('> ')

			if not rules and input not in norules: raise Exception('no rules')

			if input == 'x': 
				return
	
			elif input == 'a':
				addRule(rules)
	
			elif input == 'd':
				delRule(rules)
	
			elif input == 'p':
				G = Grammar(rules)
				G.printG()
				readInput('(press Enter to continue)')
	
			elif input == 'll':
				G = Grammar(rules)
				G.printLLTables()
				readInput('(press Enter to continue)')
		
			elif input == 'lr':
				G = Grammar(rules)
				G.printLRTables()
				readInput('(press Enter to continue)')
		
			elif input == 'gp':
				G = Grammar(rules)
				doGP(G)		

			elif input == 'pt':
				precTable()		
	
			elif input == 'r':
				G = Grammar(rules)
				rules = randomize(G)		
	
	
			elif input == 'c':
				rules = []
	
	
			elif input == 'o':
				ret = openFile(LASTFILE)
				if ret != None: rules = ret
		
			elif input == 'w':
				writeFile(rules, LASTFILE)
	
			elif input.isdigit():
				editRule(rules, int(input))
	
			else:
				raise Exception('unrecognize command "%s"' % input)


		except:
			print(FAIL('Error: ') + str(sys.exc_info()[1]) )

if len(sys.argv) > 1:
	LASTFILE = sys.argv[1]
menu()

