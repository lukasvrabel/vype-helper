#!/usr/bin/env python

# precedence table


import sys
from colors import *
from basic import printDataTable

# get table symbol for pushdown operator 'op' and input operator 'oi'
def computeTabSymbol(op, oi, p, a):
	if p[op] == p[oi]:
		if a[op] != a[oi]: 
			raise Exception('Operators "%s%" and "%s" has same priority ("%s") but different associativty (%s: %s, %s: %s)' % (op, oi, p[op], op, a[op], oi, a[oi]))
		return '>' if a[op] == 'l' else '<'
	else:
		return '>' if p[op] > p[oi] else '<'


# print precedence table for operators, their priority and associativity
#  operators - string containing one symbol for each operator (
#  priority - string containing one digit priority (higher digit = higher priority) for each operator
#  associativity - string containing 'l'/'r' for left/right asociativity for each operator
#
# Example: 
#      operators: '+-*/^'
#       priority: '11223'
#  associativity: 'llllr'
#
def printTable(operators, priority, associativity):
	p = {v: priority[i] for i, v in enumerate(operators)}
	a = {v: associativity[i] for i, v in enumerate(operators)}

	tab = {}


	header = labels = list(operators)
	data = [[computeTabSymbol(op, oi, p, a) for oi in operators] for op in operators]
	printDataTable(header, labels, data)
	

if __name__ == '__main__':
	op = '+-*/^'
	p =  '11223'
	a =  'llllr'
	printTable(op, p, a)
