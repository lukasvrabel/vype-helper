#!/usr/bin/env python

import sys
from itertools import chain
from colors import *

# basic constants and datatypes
# EPS - unicode epsilon
# CIRCLE - unicode symbol for circle denoting items
# Rule - stores rules
# FSet - frozenset with overriden __str__


# basic constants
EPS = u"\u03B5"
CIRCLE = u"\u25CF"

# class for storing and printing rule
class Rule:
	def __init__(self, left, right):
		if right == 'eps': right = EPS
		self.l = ''.join(left.split())
		self.r = ''.join(right.split())

	def __str__(self):
		str = "%s->%s" % (self.l, self.r)
		return str.encode(sys.stdout.encoding)
	def __repr__(self):
		return self.__str__()

	def __eq__(self, other): 
		return self.__dict__ == other.__dict__
	def __hash__(self):
		return hash(self.__str__())


	def getEditStr(self):
		if self.isEmpty(): return '%s->%s' % (self.l, 'eps')
		return str(self)

	def isEmpty(self):
		return True if self.r == EPS else False

	def isCNF(self):
		return True if ( len(self.r) == 2 and self.r.isupper() ) or ( len(self.r) == 1 and not self.r.isupper() ) else False

	# like string.translate
	def translate(self, table):
		return Rule(self.l.translate(table), self.r.translate(table))

	@classmethod
	def fromStr(cls, str):
		return cls( *(str.split('->')) )
		


# custom set for overriding __str__ 
class FSet(frozenset):
	def __str__(self):
		strlist = sorted([str(x) for x in super(FSet, self).__iter__()])
		return '{' + ', '.join(strlist) + '}' if len(strlist) > 0 else ''
	def __repr__(self):
		return self.__str__()



# print data table with bold header and labels
# space - space between columns
# auto - automatic column width for each column
def printDataTable(header, labels, data, space = 3, auto = False):
	#max width	
	# unique automatic width for each column 
	if auto:
		columns = zip(header, *data)
		widths = tuple( max( [len(str(x) ) + space  for x in col] ) for col in columns )
		form = ('{:<%d}' * len(widths)) % widths 

	# same width for all columns
	else:
		width = max( [len(str(x)) for x in chain(header, *data)] ) + space
		form = ('{:<%d}' % width) * len(header)

	lwidth = max( [len(str(x)) for x in labels] ) + space
	lform = '{:<%s}' % lwidth

	print( lform.format('') + BOLD(form.format(*header)) )
	for row in zip(labels, data):
		sys.stdout.write( BOLD( lform.format(row[0]) ) )
		print( form.format( *row[1] ) )
	print




if __name__ == "__main__":
	r1 = Rule('A', 'eps')
	r2 = Rule.fromStr('A -> e p s')
	print r1, r2
	rules = [
		Rule('A','BC'), Rule('B','bB'),
		Rule('B','C'), Rule('C','c')
	]
	print(FSet(rules))
