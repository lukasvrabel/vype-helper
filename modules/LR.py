#!/usr/bin/env python

import sys
from basic import *
from grammar import *

# calculations for LR table


# class for storing and printing items
class Item:
	def __init__(self, rule, index = 0):
		self.r = rule
		self.i = index

	def __str__(self):
		return str(Rule(self.r.l, self.r.r[:self.i] + CIRCLE + self.r.r[self.i:]))
	def __repr__(self):
		return self.__str__()

	def __eq__(self, other): 
		return self.__dict__ == other.__dict__
	def __hash__(self):
		return hash(self.__str__())

	# is epsilon rule
	def isEmpty(self):
		return self.r.isEmpty()

	# no more next tokens
	def hasNext(self):
		return self.i < len(self.r.r) and self.r.r[self.i] != EPS

	def getNext(self):
		if self.hasNext():
			return self.r.r[self.i]
		return None

	# jump one char and return new item
	def getJump(self):
		if not self.hasNext(): return None
		return Item(self.r, self.i + 1) 

# item set for generating closures etc
class ISet(FSet):

	def __eq__(self, other): 
		return self.__dict__ == other.__dict__

	@classmethod
	def fromRules(cls, rules):
		return cls( [Item(r) for r in rules] )

	def getClosure(self, rules):
		cl = set(super(ISet, self).__iter__())
		while True:
			newcl = set(cl)
			# loop for all items with next symbol
			for item in [it for it in cl if it.hasNext()]:
				# get rules starting with item.next
				rulesN = [rl for rl in rules if rl.l == item.getNext()]
				# create items from these rules and add them to newcl
				newcl |= ISet.fromRules(rulesN)

			# no new items - end!
			if newcl <= cl: break;
			cl = newcl

		return ISet(cl)
	
	def getStepItems(self, rules):
		items = set(super(ISet, self).__iter__())
		step = {}

		# get table of transitions
		for item in [i for i in items if i.hasNext()]:
			step.setdefault( item.getNext(), set() ).add( item.getJump() )

		# close transitions
		for symbol in step:
			step[symbol] = ISet(step[symbol]).getClosure(rules)
		
		return step
	
	

# build Item sets for Grammar
def bItemSets(G):
	start = ISet([Item(G.P[0])]).getClosure(G.P)

	itemsets = set()
	queue = [ start ]

	while queue:
		current = queue.pop()
		#print('C: %s \n Q: %s \n S %s' % (current, queue, itemsets))
		steps = current.getStepItems(G.P)
		itemsets.add( current )

		# add each new state to queue for processing
		for iset in [v for k, v in steps.items() if v not in itemsets]:
			queue.append(iset)

	return itemsets


# print states and LR table
def printTables(G):
	iset2id = { item:id for (item, id) in zip(list(G.itemSets), range(len(G.itemSets))) }
	id2iset = { id:item for item, id in iset2id.items() }

	rule2id = { r:id for id, r in enumerate(G.P) }

	# print states
	print "States:"
	for i in range(len(id2iset)):
		print i, id2iset[i]
	
	# prepare empty table
	# indexed by string symbol+id
	table = {}
	for id in id2iset:
		for symbol in G.N | G.T | {'$'}:
			table[symbol+str(id)] = []

	# build table
	for id, items in id2iset.items():
		for i in items:
			if not i.hasNext():
				for symbol in G.follow[i.r.l]:
					table[symbol+str(id)].append('r'+str(rule2id[i.r]))
			else:
				symbol = i.getNext()
				nextid = iset2id[ items.getStepItems(G.P)[symbol] ]
				table[symbol+str(id)].append(nextid)


	
	# print table
	form =("{:>8}") 
	labels = ("{:>5}")
	
	header = sorted(list(G.T)) + ['$'] + sorted(list(G.N))
	labels = sorted(id2iset.keys())
	data = [ [ FSet( table[symbol + str(id)] ) for symbol in header ] for id in labels ]
	printDataTable(header, labels, data)
	return 

	# header
	symbols = sorted(list(G.T)) + ['$'] + sorted(list(G.N))
	print
	sys.stdout.write(labels.format(''))
	for symbol in symbols:
		sys.stdout.write(form.format(symbol))
	print 

	#body
	for id, items in id2iset.items():
		#sys.stdout.write(labels.format(str(id) + ': ' + str(items)))
		sys.stdout.write(labels.format(str(id)))
		for symbol in symbols:
			sys.stdout.write(form.format(FSet(table[symbol+str(id)])))
		print
	print



if __name__ == "__main__":
	r1 = Rule('A', 'eps')
	r2 = Rule.fromStr('A -> e p s')
	i1 = Item(r1)
	i2 = Item(r2, 1)
	print i1, i1.getNext()
	print i2, i2.getNext()

	rules = [
		Rule('A','BC'), Rule('B','bB'),
		Rule('B','C'), Rule('C','c')
	]
	print(FSet(rules))
	s = ISet.fromRules(rules[0:1])
	c = s.getClosure(rules)
	print s
	print c
	test = ISet([Item(rules[0]), Item(rules[1], 1)])
	print test
	print test.getStepItems(rules)
#	print bItems(Grammar(rules))
	printTables(Grammar(rules))
