#!/usr/bin/env python

import sys
from basic import *
import LL
import LR
import GP

# stores grammar and results of cumputations for LL and LR tables
# constructed from list of rules (Rule)
# 
# N, T, P, S - components
# empty, first, follow, predict, itemSets - computed sets used for LL and LR tables
# printG - print grammar
# printLLTables, printLRTables - compute and print tables

class Grammar:
	def __init__(self, rules):
		
		N = set()
		T = set()
		for r in rules:
			N.add(r.l)
			if r.isEmpty(): continue
			for c in r.r:
				if c.isupper(): N.add(c)
				else: T.add(c)
	
		self.N = FSet(N)
		self.T = FSet(T)
		self.P = rules #FSet(rules)
		self.S = rules[0].l

		self._empty = None
		self._first = None
		self._follow = None
		self._predict = None 
		self._items = None 
	
	def __str__(self):
		str = '(N: %s, T: %s, P: %s, S: %s)' % (self.N, self.T, self.P, self.S)
		return str

	def isCNF(self):
		return all([r.isCNF() for r in self.P])

	def printG(self):
		print('N: %s' % self.N)
		print('T: %s' % self.T)
		print('P: %s' % self.P)
		print('S: %s' % self.S)

	# LL stuff
	@property
	def empty(self):
		if not self._empty: self._empty = LL.bEmpty(self)
		return self._empty

	@property
	def first(self):
		if not self._first: self._first = LL.bFirst(self)
		return self._first

	@property
	def follow(self):
		if not self._follow: self._follow = LL.bFollow(self)
		return self._follow

	@property
	def predict(self):
		if not self._predict: self._predict = LL.bPredict(self)
		return self._predict

	# LR stuff
	@property
	def itemSets(self):
		if not self._items: self._items = LR.bItemSets(self)
		return self._items


	def printLLTables(self):
		LL.printTables(self)

	def printLRTables(self):
		LR.printTables(self)

	def printGPParse(self, string):
		GP.printParse(self, string)



if __name__ == "__main__":
	rules = [Rule('A', 'BC'), Rule('B', 'aC'), Rule('B', 'b'), Rule('C', 'D'), Rule('D', 'fG'), Rule('D', 'd'), Rule('E', 'fGH')]
	rules += [Rule('B', 'eps'), Rule('D', 'eps')]
	G = Grammar(rules)
	G.printG()
	LL.printTables(G)
