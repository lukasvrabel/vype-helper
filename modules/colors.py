
# ansi terminal colors
class COLOR:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	BOLD = "\033[1m"
	ENDC = '\033[0m'
	
	def disable(self):
		self.HEADER = ''
		self.OKBLUE = ''
		self.OKGREEN = ''
		self.WARNING = ''
		self.FAIL = ''
		self.ENDC = ''

# procedures for returning colored strings
def OK(str):
	return COLOR.OKGREEN + str + COLOR.ENDC
def BLUE(str):
	return COLOR.OKBLUE + str + COLOR.ENDC
def FAIL(str):
	return COLOR.FAIL + str + COLOR.ENDC
def WARN(str):
	return COLOR.WARNING + str + COLOR.ENDC
def BOLD(str):
	return COLOR.BOLD + str + COLOR.ENDC
