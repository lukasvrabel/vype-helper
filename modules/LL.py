#!/usr/bin/env python
from basic import *
import sys

# calculations for LL table


# build empty set
def bEmpty(G):
	empty = set( [r.l for r in G.P if r.isEmpty()] )
	while True:
		next = set( [r.l for r in G.P if set(r.r) <= empty] )
		if next <= empty: break
		empty |= next

	# check method for string
	e = FSet(empty)
	e.isEmpty = lambda x: True if set(x) <= (empty | {EPS}) else False
	return e





# build First set from grammar
def bFirst(G):
	# initial sets
	first = {}
	for c in G.T:
		first[c] = set(c)
	for c in G.N:
		first[c] = set()


	# loop until nothing change
	change = True
	while change:
		change = False
		# for each non-empty rule
		for rule in [r for r in G.P if not r.isEmpty()]:

			new = getFirst(rule.r, G.empty, first)
			if not new <= first[rule.l]:
				change = True
				first[rule.l] |= new

	# froze sets for better formatting and hashing
	first = {k: FSet(v) for k, v in first.items()}
	return first

# get 'First' set from string
def getFirst(str, empty, first):
	#return FSet().union(*[ first[c] for i, c in enumerate(str) if empty.isEmpty(str[:i]) ])
	if str == EPS: return set()
	newfirst = set()
	for symbol in str:
		newfirst |= first[symbol]
		if symbol in empty:
			continue
		else:
			break
	return newfirst


def bFollow(G):
	follow = {}
	for n in G.N: follow[n] = set()
	# start symbol have $ in follow
	follow[G.S] = set('$')	

	change = True
	while change:
		change = False
		# loop for non-empty rules
		for rule in [r for r in G.P if not r.isEmpty()]:
			for i, symbol in enumerate(rule.r):
				# we are interested only in nonterminals (G[0])
				if symbol not in G.N: continue

				new = getFirst(rule.r[i+1:], G.empty, G.first)
				if G.empty.isEmpty(rule.r[i+1:]):
					new |= follow[rule.l]

				if not new <= follow[symbol]:
					change = True
					follow[symbol] |= new

	# froze sets for better formatting and hashing
	follow = {k: FSet(v) for k, v in follow.items()}
	return follow


def bPredict(G):
	predict = {}
	for rule in G.P:
		predict[rule] = getFirst(rule.r, G.empty, G.first)
		if G.empty.isEmpty(rule.r):
			predict[rule] |= G.follow[rule.l]

	# froze sets for better formatting and hashing
	predict = {k: FSet(v) for k, v in predict.items()}
	return predict



def printTables(G): #, empty, first, follow, rules, predict):

	### empty, first, follow
	print	
	# convert empty set to dict
	e = {n: True for n in G.empty}
	e.update((n, False) for n in G.N if n not in G.empty)
	

	header = ['Empty', 'First', 'Follow']
	# sort nonterminals for labels
	labels = sorted(list(G.N))
	# get data for each nonterminal
	data = [[e.get(n), G.first.get(n), G.follow.get(n)] for n in labels]
	printDataTable(header, labels, data, auto=True)

	### predict
	print
	print("Predict")

	collen = 5 + max([len(str(r)) for r in G.P])
	form =("{:<%d}" % collen) * 2
	for i, r in enumerate(G.P):
		print(form.format('%d: %s' % (i, r), FSet(G.predict[r])))


	### LL Table
	print
	print("LL Table")

	# initialize empty LL table
	# index is string containing nonterminal and terminal
	LLtab = {k: set() for k in [n+t for t in (G.T | {'$'}) for n in G.N]}
	for i, r in enumerate(G.P):		
		for symbol in G.predict[r]:
			LLtab[r.l + symbol].add(i)

	terms = sorted(list(G.T)) + ['$']
	nonterms = sorted(list(G.N))
	data = [ [FSet(LLtab[n+t]) for t in terms] for n in nonterms ] 
	printDataTable(terms, nonterms, data)

