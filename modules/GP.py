#!/usr/bin/env python
# General parsing based on chomsky normal form


from itertools import product
from basic import *
from grammar import *


class S(dict):

	# override [] to behave like setdefault with empty set
	def __getitem__(self, key):
		return self.setdefault(key, set())

	# if low or hight is specified, then get all keys containing low/high
	def getKeySplice(self, low = None, high = None):
		if low == None and high == None:
			return list(self)
		if low == None:
			return [k for k in self if k[1] == high]
		if high == None:
			return [k for k in self if k[0] == low]
		else:
			return [k for k in self if k == (low, high)]

	# get all keys in form (low, x) (x+1, high)
	def getKeyPairs(self, low, high):
		pairs = []
		lows = self.getKeySplice(low, None)
		#print('getKeyPairs', low, high, lows)
		for left in lows:
			right = (left[1]+1, high)
			if self.has_key(right): pairs.append( (left, right) )

		return pairs

	# get all left-hand side nonterminals for pairs first, second
	# firs -
	def getLeftNT(self, first, second, r2l):
		"""Get all left-hand side nonterminals for given pairs
			
		Arguments:
		first - key pair for set of first nonterminal
		second - key pair for set of second nonterminal
		r2l - dictionary containing all left-hand side nonterminals
			for right-hand side string

		if A in first and B in second, then r2l['AB'] will be included in returned set
		"""

		# set of first and second nonterminals
		f = self[first]
		s = self[second]

		# all possible right sides
		rights = FSet([i[0] + i[1] for i in product(f, s)])
		lefts = FSet.union( FSet(),  *[v for k, v in r2l.items() if k in rights] )
		return lefts

	# get all left-hand side nonterminals for given pair
	def getPairSet(self, pair, r2l):
		# first get all lower pairs
		lowerPairs = self.getKeyPairs(pair[0], pair[1])
		#print ("getSet for ", pair, lowerPairs)
		left = set()
		for p in lowerPairs:
			left |= self.getLeftNT(p[0], p[1], r2l)

		return FSet(left)


def printParse(G, string):
	# only first 10 symbols are considered

	string = string[:10]

	# left-hand sides of rules indexed by their right-hand side
	# r2l = right to left (right as index, left as value)
	r2l = {rule.r: set() for rule in G.P}
	for rule in G.P: r2l[rule.r].add(rule.l)
	r2l.update((k, FSet(v)) for k, v in r2l.items())

	s = S()

	# initial terminal rules
	for i, symbol in enumerate(string):
		s[(i, i)] = r2l[symbol]
		print('S%s: %s' % ((i, i), s[(i, i)]))
	
	print
	
	for dist in range(1, len(string)):
		level = [(i, i+dist) for i in range(0, len(string)-dist)]
		for pair in level:
			left = s.getPairSet(pair, r2l)
			s[pair] = left
			print('S%s: %s' % (pair, left))
		print


		

if __name__ == '__main__':
	rules = [
		Rule('S','AB'), Rule('A','a'),
		Rule('B','BB'), Rule('B','b'),
		Rule('B','AB'), Rule('A','b'),
	]
	G = Grammar(rules)
	G.printG()

	# create r2l from rules
	r2l = {rule.r: set() for rule in G.P}
	for rule in G.P: r2l[rule.r].add(rule.l)
	r2l.update((k, FSet(v)) for k, v in r2l.items())

	keys = {
		(1,1):{'A','B'}, (2, 2):{'B','C'}, (3, 3):{'C'}, (4, 4):{'C'}, 
		(1, 2):{'A'}, (2, 3):{'B'}, (3, 4):{'C'},
		(1, 3):{'A'}, (2, 4):{'B'},
		(1, 4):{'A'}
	}
	s = S(keys)
	print s.getKeySplice(1, None)
	print s.getKeySplice(None, 3)
	print s.getKeyPairs(1, 3)
	print s.getKeyPairs(2, 4)
	print s.getKeyPairs(1, 4)
	print s.getLeftNT((1,1), (2,2), r2l)

	printParse(G, 'aabb')
